1. Look at structere of a Rails application

2. Brief look at MVC (Model, View, Controller)

3. Explore how to set root route to a page of our choice

4. Explore the main layout file - application.html.erb

5. Explore embedded ruby code within .html.erb files

6. We will have courses in our university that students can enroll in

7. Courses will have short name, name, description

8. The index page will have a listing of courses that are available

9. Let's add some placeholder courses

10. Let's talk about styling the app - frameworks

11. Bootstrap, Materialize

12. Add the materialize gem

13. Add navigation 

14. Add footer, make it sticky!

15. Add footer, make it sticky

16. Add menu icon (learn how to use icon)

17. Add js functionality - dropdown menu items

18. Improve mobile js functionality - sidenav trigger

